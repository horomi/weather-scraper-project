from flask import Flask, render_template, request
from scraping_weather import check_temp

app = Flask(__name__)


@app.route('/')
def home():
    return render_template('home.html', the_title='Welcome to the Weather London app!')


@app.route('/search', methods=['POST'])
def check_weather():
    day = request.form['weekday']
    return render_template('result.html', weekday=day, result=check_temp(day))


@app.route('/result')
def result():
    return render_template('result.html')


if __name__ == "__main__":
    app.run()