from datetime import date


def create_dictionary_days_of_week():
    """Function created a dictionary where a key is day of week,
    value - specific id from html tag "li" in source page """
    days_of_the_week = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]  # [0,1,2,3,4,5,6]
    current_date = date.today()

    dict_days_of_week = {}
    i = current_date.weekday()
    tab_day = 0

    while tab_day < 7:
        if i > len(days_of_the_week) - 1:
            i = 0
        dict_days_of_week[days_of_the_week[i]] = "tabDay" + str(tab_day)
        tab_day += 1
        i += 1
    return dict_days_of_week
