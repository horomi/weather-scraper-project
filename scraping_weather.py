import requests
from bs4 import BeautifulSoup
from code import create_dictionary_days_of_week

# headers = {
#     "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
#     "User-agent": "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Mobile Safari/537.36"
#     }
#
# url = "https://www.metoffice.gov.uk/weather/forecast/gcpvj0v07#?date=2022-10-11"
# req = requests.get(url, headers=headers)
#
# src = req.text
#
# with open("index.html", "w") as file:
#     file.write(src)

with open("index.html") as file:
    src = file.read()

soup = BeautifulSoup(src, "lxml")


def check_week_temp(week):
    weather_week_list = []
    id_days_dict = create_dictionary_days_of_week()
    for day in id_days_dict:
        id_user_day = id_days_dict[day]
        ff = soup.find("li", {"id": id_user_day}).find("time")
        day_of_week = ff.text.strip()
        max_temperature = soup.find("li", {"id": id_user_day}).find("span", class_="tab-temp-high")
        max_temp = max_temperature.text.strip()
        min_temperature = soup.find("li", {"id": id_user_day}).find("span", class_="tab-temp-low")
        min_temp = min_temperature.text.strip()
        weather_week_list.append((day_of_week, max_temp, min_temp))
    return weather_week_list


# print(check_week_temp("week"))


def check_temp(day):
    day = day.lower()
    if day == "week":
        return check_week_temp(day)
    else:
        id_days_dict = create_dictionary_days_of_week()
        id_user_day = id_days_dict[day]
        ff = soup.find("li", {"id": id_user_day}).find("time")
        day_of_week = ff.text.strip()
        max_temperature = soup.find("li", {"id": id_user_day}).find("span", class_="tab-temp-high")
        max_temp = max_temperature.text.strip()
        min_temperature = soup.find("li", {"id": id_user_day}).find("span", class_="tab-temp-low")
        min_temp = min_temperature.text.strip()

        return day_of_week + ":", max_temp, min_temp

# print(check_temp("week"))